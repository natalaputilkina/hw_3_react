import './ProductsList.scss'
import ProductCard from "../../components/ProductCard/ProductCard";

const ProductsList = ({ data, addToCart, removeCart, addToFavorite, favorite, cart }) => {
    return (
        <div className="products-list">
            {data.map((product) => (
                <ProductCard
                    key={product.id}
                    product={product}
                    addToCart={addToCart}
                    removeCart={removeCart}
                    addToFavorite={addToFavorite}
                    isFavorite={favorite.some((item) => item.id === product.id)}
                    isInCart={cart.some((item) => item.id === product.id)}
                />
            ))}
        </div>
    );
};

export default ProductsList;