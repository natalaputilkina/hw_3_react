import { useOutletContext } from 'react-router-dom';
import './Cartlist.scss'
import ProductsList from '../ProductsList/ProductsList.jsx';





function CartList(){

    const { cart, addToCart, addToFavorite, favorite, removeCart, products } = useOutletContext();

    // const cart = JSON.parse(localStorage.getItem('cart')) || []

    if (cart.length === 0){
        return (

            <>
                <h1>Cart</h1>
                <h2>Ваш кошик порожній, але можете його поповнити</h2>

            </>
        
        )
    }

    const cartProducts = products.filter((product) => cart.some((cartItem) => cartItem.id === product.id));

    console.log(cartProducts)


    return (
        
            <>
                <h1 className='title'>Cart</h1>
                <ProductsList
                data={cartProducts}
                addToCart={addToCart}
                removeCart={removeCart}
                addToFavorite={addToFavorite}
                favorite={favorite}
                cart={cart}
                 />
            </>
        
    )

        
    
}

export default CartList
