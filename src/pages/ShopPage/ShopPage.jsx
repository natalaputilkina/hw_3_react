import { useOutletContext } from 'react-router-dom';
import "./ShopPage.scss"
import ProductsList from '../ProductsList/ProductsList.jsx';




function ShopPage(){

   

    const { products, addToCart, addToFavorite, favorite, removeCart, cart } = useOutletContext();

     return (
        <>
            <h1 className='title'>Shop page</h1>
            <div className="shop--page">
                <ProductsList
                    className="shop--page"
                    data={products}
                    addToCart={addToCart}
                    removeCart={removeCart}
                    addToFavorite={addToFavorite}
                    favorite={favorite}
                    cart={cart}
                />
            </div> 
        </>   
                  

    )
    

}
export default ShopPage