import { BrowserRouter, Routes, Route, Link } from "react-router-dom";
import Layout from "./Layout"
import WishList from "./components/Wishlist/Wishlist";
import CartList from "./components/Cart/Cartlist"
import './App.scss'


function App(){
  return (
    <BrowserRouter>
        <div className="header-conteiner">

            <div className='header-menu'>

                <Link className="header-link" to="/">Shop</Link>
                <Link className="header-link" to="/wishlist">Wish-list</Link>
                <Link className="header-link" to="/cart">Cart</Link>

            </div>   

          <Routes>

            <Route path="/" element={<Layout/>}/>
            <Route path="/wishlist" element={<WishList />}/>
            <Route path="/cart" element={<CartList />}/>
            

            
          </Routes>


        </div>
    </BrowserRouter>
  )
}

export default App;
