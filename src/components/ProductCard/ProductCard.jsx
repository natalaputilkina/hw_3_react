import "./ProductCard.scss"
import ModalPage from "../../pages/MainPage/ModalPage";
import IconStar from "../../assets/star.svg"
import starFavoritIcon from "../../assets/star-favorit.svg";
import Button from "../Button";




function ProductCard ({product, addToCart, addToFavorite, isFavorite, removeCart, isInCart}){
        

        const {id, name, imageUrl, price, color} = product    
    return (
        <div className="product">
            <h2 className="product--name">{name}</h2>
            <img className="product--img" src={imageUrl}></img> 
            <p className="product--price">Price {price} $</p>
            <p className="product--article">Article {id}</p>
            <div className="product--color">{color}</div>
            {isInCart ? (
                <Button classNames="button_modal" disabled title="In cart" />
            ) : (
                <ModalPage 
                    type="first" 
                    title="Add to cart" 
                    text={`By clicking the “Yes, add” button, ${name} will be added to cart.`} 
                    secondaryText="YES, ADD" 
                    click={() => addToCart(id)}
                />
            )}
            {/* <ModalPage type = "first" title="Add to cart" text="By clicking the “Yes, add” button, PRODUCT will be add to cart." secondaryText="YES, ADD" click={() => addToCart(id)}/> */}
            <button className="favorite__button" onClick={() => addToFavorite(id)}>
                <img className="favorite--image" src={isFavorite ? starFavoritIcon : IconStar} alt="Favorite" />
            </button>

            {isInCart && (
                <ModalPage 
                    type="first" 
                    title="Remove from cart" 
                    text={`By clicking the “Yes, remove” button, ${name} will be removed from cart.`} 
                    secondaryText="YES, REMOVE" 
                    click={() => removeCart(id)}
                />
            )}
            {/* <ModalPage type = "first" title="Remove from cart" text="By clicking the “Yes, remove” button, PRODUCT will be remove from cart." secondaryText="YES, REMOVE" click={() => removeCart(id)}/> */}
        </div>
    )
}


export default ProductCard