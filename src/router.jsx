import { createBrowserRouter } from "react-router-dom";
import Layout from './Layout';
import ShopPage from "./pages/ShopPage/ShopPage";
import WishList from "./pages/Wishlist/Wishlist";
import CartList from "./pages/Cart/Cartlist";


export const router = createBrowserRouter([
    {
        path: '/',
        element: <Layout />,
        children: [
            {
                index: true,
                element: <ShopPage />
            },         
            {
                path: '/wishlist',
                element: <WishList />
            },
            {
                path: '/cart',
                element: <CartList />
            }
        ]
    }
]);
